// 1.setTimeout - функція викликається тільки один раз,
//   setInterval - функція викликається регулярно через певний відрізок часу
// 2. якщо передати 0 затримку, то функція викликається дуже швидко,
// але після завершення виконання поточного коду.
// 3.Тому що clearInterval припиняє подальші виклики функції.


const img = document.querySelectorAll(".image-to-show");

const button = document.querySelector(".stop");

const btn = document.querySelector(".restore")


let number = 0;


let showImg = function() {

    img.forEach((image,index) => {

        if(index === number) {
            image.style.display = "block";
        } else {
            image.style.display = "none";
        };
    });

    console.log(number);

    number = number + 1;
    
    if(number === 4) {
        number = 0;
    };
};


let timerId = setInterval(showImg, 3000);

button.addEventListener("click", () => {

    clearInterval(timerId);
    timerId = null;
})

btn.addEventListener("click", () => {

    if(timerId === null) {

    timerId = setInterval(showImg, 3000);

    };
})
